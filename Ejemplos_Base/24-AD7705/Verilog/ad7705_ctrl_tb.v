/*------------------------------------------------------------------------
---- Module:  ad7705_ctrl_tb                                          ----
---- Description: Testbench AD7705 FSM                                ----
---- Author/s:  Martín A. Heredia                                     ----
---- Last revision: 09/04/2023                                        ----
---- Dependencies:                                                    ----
----     parameters.v                                                 ----
------------------------------------------------------------------------*/

`timescale 1 ns/100 ps
`define SIMULATION
`include "ad7705_ctrl.v"

module ad7705_ctrl_tb();

//Parámetros y definiciones del test:
localparam PER2 = 10.0;

//Señales internas:
reg                  test_clk_in     ;  
reg                  test_rst_in_n   ;  
reg                  test_drdy_in_n  ;  
reg                  test_MISO_in    ;  
wire                 test_MOSI_out   ; 
wire                 test_cs_out     ; 
wire                 test_sclk_out   ; 
wire                 test_dready_out ; 
wire                 test_rst_out_n  ;
wire [`DATABITS-1:0] test_data_out   ; 

//DUT:
ad7705_ctrl dut
(
    .clk_in      (test_clk_in     ),
    .rst_in_n    (test_rst_in_n   ),
    .mux_sel_in_n( 1'b0           ),
    .drdy_in_n   (test_drdy_in_n  ),
    .MISO_in     (test_MISO_in    ),
    .MOSI_out    (test_MOSI_out   ),
    .cs_out      (test_cs_out     ),
    .sclk_out    (test_sclk_out   ),
    .dready_out  (test_dready_out ),
    .rst_out_n   (test_rst_out_n  ),
    .data_out    (test_data_out   ),
    .leds_out    (                )
);

//Clock:
always
begin
    test_clk_in = 1'b1;
    #PER2;
    test_clk_in = 1'b0;
    #PER2;
end

//Test
initial 
begin
    $dumpfile("test.vcd");
    $dumpvars(0, ad7705_ctrl_tb);
    //Inicio del Test
    $display("ad7705_ctrl_tb start...");
    //Reset
    $display("Reset");
    test_rst_in_n  <= 1'b0;
    test_drdy_in_n <= 1'b1;
    test_MISO_in   <= 1'b1;
    #(PER2*20);
    test_rst_in_n <= 1'b1;

    #6000.0;

    test_drdy_in_n <= 1'b0;
    #(PER2*8);
    test_drdy_in_n <= 1'b1;
    #2000.0;
    test_drdy_in_n <= 1'b0;
    #(PER2*8);
    test_drdy_in_n <= 1'b1;

    #6000.0
   
    //Fin del test
    $display("Test finished");
    $finish;
end

endmodule