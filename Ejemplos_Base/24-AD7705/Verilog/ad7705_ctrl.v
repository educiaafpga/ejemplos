/*------------------------------------------------------------------------
---- Module:  ad7705_ctrl                                             ----
---- Description: FSM to configure and get samples from AD7705 ADC    ----
---- Author/s:  Martín A. Heredia                                     ----
---- Last revision: Sep. 2023                                         ----
---- Dependencies:                                                    ----
----     spi.v                                                        ----
------------------------------------------------------------------------*/

`include "spi.v"
`include "ad7705_params.v"

module ad7705_ctrl
(
    input             clk_in       ,
    input             rst_in_n     ,
    input             mux_sel_in_n ,
    input             drdy_in_n    ,
    input             MISO_in      ,
    output            MOSI_out     ,
    output            cs_out       ,
    output            sclk_out     ,
    output reg        dready_out   ,
    output            rst_out_n    ,
    output reg [15:0] data_out     ,
    output reg [3:0]  leds_out      
);

//Configuración del AD7705
`define CLK_CONFIGURATION    {1'b0, `CLOCK_REG, `WRITE, 1'b0, `CHANNEL_0, 4'b0000, `CLKDIV, `O_RATE_50HZ} // 0x200C
`define SETUP_CONFIGURATION  {1'b0, `SETUP_REG, `WRITE, 1'b0, `CHANNEL_0, `SELF_CALIBRATION_MODE, `GAIN_X1, `UNIPOLAR_OPERATION, `BUFFER_SHORTED_OUT, `FSYNC_LOW} //0x1044
`define CFG_READ_DATA        {1'b0, `COMM_REG , `WRITE, 1'b0, `CHANNEL_0, 1'b0, `DATA_REG , `READ, 1'b0, `CHANNEL_0} //0X0038

//Parámetros locales
localparam DATABITS      = `DATABITS;
localparam NBITS_CLK_DIV = 2;

//Señales internas
//Registro de entrada:
reg dready_in_n_reg;

//SPI:
wire spi_MOSI                  ;
wire spi_sclk                  ;
wire spi_cs                    ;
wire spi_dataReceived_s        ;
wire spi_shifting_s            ;
reg  spi_write_s               ;
reg  spi_read_s                ;
reg  [DATABITS-1:0] spi_data_i ;
wire [DATABITS-1:0] spi_data_o ;

//Divisor de clock y Global Buffer:
reg [NBITS_CLK_DIV-1:0] div_cntr  ;
wire                    glbl_clk  ;

//FSM:
localparam          IDLE                 = 3'b000 ;
localparam          CLK_CFG              = 3'b001 ;
localparam          SETUP_CFG            = 3'b010 ;
localparam          WAIT_FOR_SAMPLE      = 3'b011 ;
localparam          SET_CFG_TO_READ      = 3'b100 ;
localparam          READ_SAMPLE          = 3'b101 ;
localparam          WAIT_ON_CLK_CFG      = 3'b110 ;
localparam          WAIT_ON_SETUP_CFG    = 3'b111 ;
reg  [2:0]          currentState_s                ;
reg  [2:0]          nextState_s                   ;

//Descripción de la lógica:

//Registro de entrada drdy_in_n
always @ (posedge glbl_clk, negedge rst_in_n)
begin
    if(!rst_in_n)
    begin
        dready_in_n_reg <= 1'b1;
    end
    else
    begin
        dready_in_n_reg <= drdy_in_n;
    end
end

//Salidas
always @ (posedge glbl_clk, negedge rst_in_n)
begin
    if(!rst_in_n)
    begin
        data_out <= {DATABITS{1'b0}};
    end
    else if(spi_read_s && spi_dataReceived_s)
    begin
        data_out <= spi_data_o;
    end
end

always @ (posedge glbl_clk, negedge rst_in_n)
begin
    if(!rst_in_n)
        leds_out <= 4'b0000;
    else if(mux_sel_in_n)
        leds_out <= spi_data_o[DATABITS-1-:4];
    else
        leds_out <= spi_data_o[DATABITS-1-4-:4];
end

always @ (posedge glbl_clk, negedge rst_in_n)
begin
    if(!rst_in_n)
        dready_out <= 1'b0;
    else 
        dready_out <= (spi_read_s & spi_dataReceived_s);
end

assign rst_out_n   = rst_in_n   ;
assign MOSI_out    = spi_MOSI   ;
assign cs_out      = spi_cs     ;
assign sclk_out    = spi_sclk   ;

//Interfaz SPI
spi
#(
    .dataBits         (DATABITS  ),
    .cPol             (`CPOL     ),
    .cPha             (`CPHA     ),
    .csPol            (`CSPOL    )
)
spi_0
(
    .clk_in           (glbl_clk           ),
    .rst_in_n         (rst_in_n           ),
    .drdy_in_n        (dready_in_n_reg    ),
    .serialD_in       (MISO_in            ),
    .serialD_out      (spi_MOSI           ),
    .cs_out           (spi_cs             ),
    .sclk_out         (spi_sclk           ),
    .dataReceived_out (spi_dataReceived_s ),
    .shifting_out     (spi_shifting_s     ),
    .write_in         (spi_write_s        ),
    .read_in          (spi_read_s         ),
    .data_in          (spi_data_i         ),
    .data_out         (spi_data_o         )
);

//Divisor de clock
always @ (posedge clk_in, negedge rst_in_n)
begin
    if(!rst_in_n)
        div_cntr <= {NBITS_CLK_DIV{1'b0}};
    else
        div_cntr <= div_cntr+1;
end

//Buffer Global
`ifndef SIMULATION
SB_GB sbGlobalBuffer_inst0
( 
    .USER_SIGNAL_TO_GLOBAL_BUFFER (div_cntr[NBITS_CLK_DIV-1]),
    .GLOBAL_BUFFER_OUTPUT         (glbl_clk                 )  
);
`else
  assign glbl_clk = div_cntr[NBITS_CLK_DIV-1];
`endif

//Parte secuencial de la FSM
always @ (posedge glbl_clk, negedge rst_in_n)
begin
    if(!rst_in_n)
        currentState_s <= IDLE;
    else 
        currentState_s <= nextState_s;
end

//Parte combinacional de la FSM
always @(*)
begin
    case(currentState_s)
        IDLE:
        begin
            nextState_s    = CLK_CFG          ;
            spi_write_s    = 1'b0             ;
            spi_read_s     = 1'b0             ;
            spi_data_i     = {DATABITS{1'b0}} ;
        end
        
        CLK_CFG:
        begin
            if(spi_shifting_s)
                nextState_s = WAIT_ON_CLK_CFG;
            else
                nextState_s = CLK_CFG;
            spi_write_s    = 1'b1                 ;
            spi_read_s     = 1'b0                 ;
            spi_data_i     = `CLK_CONFIGURATION   ;
        end

        SETUP_CFG:
        begin
            if(spi_shifting_s)
                nextState_s = WAIT_ON_SETUP_CFG;
            else
                nextState_s = SETUP_CFG;
            spi_write_s    = 1'b1                 ;
            spi_read_s     = 1'b0                 ;
            spi_data_i     = `SETUP_CONFIGURATION ;
        end
    
        WAIT_FOR_SAMPLE:
        begin
            if(!dready_in_n_reg)
                nextState_s = SET_CFG_TO_READ;
            else
                nextState_s = WAIT_FOR_SAMPLE;
            spi_write_s    = 1'b0             ;
            spi_read_s     = 1'b0             ;
            spi_data_i     = {DATABITS{1'b0}} ;
        end

        SET_CFG_TO_READ:
        begin
            if(spi_dataReceived_s)
                nextState_s = READ_SAMPLE;
            else
                nextState_s = SET_CFG_TO_READ;
            spi_write_s    = 1'b1            ;
            spi_read_s     = 1'b0            ;
            spi_data_i     = `CFG_READ_DATA  ;
        end
    
        READ_SAMPLE:
        begin
            if(spi_dataReceived_s)
                nextState_s = WAIT_FOR_SAMPLE;
            else
                nextState_s = READ_SAMPLE;
            spi_write_s    = 1'b0             ;
            spi_read_s     = 1'b1             ;
            spi_data_i     = {DATABITS{1'b0}} ;
        end 
        
        WAIT_ON_CLK_CFG:
        begin
            if(spi_dataReceived_s)
                nextState_s = SETUP_CFG;
            else
                nextState_s = WAIT_ON_CLK_CFG;
            spi_write_s    = 1'b0                 ;
            spi_read_s     = 1'b0                 ;
            spi_data_i     = {DATABITS{1'b0}}     ;
        end 

        WAIT_ON_SETUP_CFG:
        begin
            if(spi_dataReceived_s)
                nextState_s = WAIT_FOR_SAMPLE;
            else
                nextState_s = WAIT_ON_SETUP_CFG;
            spi_write_s    = 1'b0                 ;
            spi_read_s     = 1'b0                 ;
            spi_data_i     = {DATABITS{1'b0}}     ;
        end 

        default:
        begin
            nextState_s    = IDLE             ;
            spi_write_s    = 1'b0             ;
            spi_read_s     = 1'b0             ;
            spi_data_i     = {DATABITS{1'b0}} ;
        end
    endcase;
end
endmodule