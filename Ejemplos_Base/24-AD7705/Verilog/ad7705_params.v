`ifndef _AD7705_PARAMS_
`define _AD7705_PARAMS_

//Interfaz de comunicación
`define DATABITS  16   
`define CPOL      1'b1 
`define CPHA      1'b1 
`define CSPOL     1'b0 

//Registros internos
`define COMM_REG   3'b000
`define SETUP_REG  3'b001
`define CLOCK_REG  3'b010
`define DATA_REG   3'b011
`define TEST_REG   3'b100
`define ZSC_REG    3'b110
`define FSC_REG    3'b111

//Lectura/Escritura
`define READ   1'b1
`define WRITE  1'b0 

//Canales
`define CHANNEL_0  2'b00
`define CHANNEL_1  2'b01
`define CHANNEL_2  2'b10
`define CHANNEL_3  2'b11

//Modos de funcionamiento
`define NORMAL_MODE                  2'b00
`define SELF_CALIBRATION_MODE        2'b01
`define ZERO_SCALE_CALIBRATION_MODE  2'b10
`define FULL_SCALE_CALIBRATION_MODE  2'b11

//Ganancia
`define GAIN_X1   3'b000
`define GAIN_X2   3'b001
`define GAIN_X4   3'b010
`define GAIN_X8   3'b011
`define GAIN_X16  3'b100
`define GAIN_X32  3'b101
`define GAIN_X64  3'b110
`define GAIN_X128 3'b111

//Bipolar/Unipolar
`define BIPOLAR_OPERATION  1'b0
`define UNIPOLAR_OPERATION 1'b1

//Buffer control
`define BUFFER_SHORTED_OUT  1'b0
`define BUFFER_ON           1'b1

//Sincronización del filtrado
`define FSYNC_HIGH  1'b1
`define FSYNC_LOW   1'b0

//Clock setup
`define CLKDIV        1'b1  
`define O_RATE_20HZ   3'b000
`define O_RATE_25HZ   3'b001
`define O_RATE_100HZ  3'b010
`define O_RATE_200HZ  3'b011
`define O_RATE_50HZ   3'b100
`define O_RATE_60HZ   3'b101
`define O_RATE_250HZ  3'b110
`define O_RATE_500HZ  3'b111

`endif 