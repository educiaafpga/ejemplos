# AD7705

[[_TOC_]]

## 1. Descripción

Aquí se mostrará un caso de aplicación del periférico SPI visto en [23-SPI](../23-SPI/), el cual fue [implementado nuevamente en Verilog](./Verilog/spi.v) para este ejemplo. 

La aplicación se basará en el conversor analógico-digital [AD7705 de Analog Devices](https://www.analog.com/media/en/technical-documentation/data-sheets/AD7705_7706.pdf). El objetivo será implementar un módulo que lea las muestras provenientes del AD7705 y las entregue a su salida.

### Introducción

El AD7705 es un conversor analógico-digital (ADC) de 16 bits del tipo *Sigma-Delta*. Algunas de sus características se listan a continuación:

- Interfaz SPI e I2C.
- Capaz de trabajar a 3.3V o 5V.
- 2 canales diferenciales.
- Ganancia de entrada programable.

Su diagrama en bloques se muestra a continuación:

![ad7705_bd.png](.images/ad7705_bd.png)

En el diagrama anterior se observan los puertos de entrada y salida digitales del ADC que serán conectados a la EDU-CIAA-FPGA para configuración y lectura de datos. Estos puertos son:

- **SCLK**: entrada de clock SPI.
- **CS**: entrada (negada) de *chip select* o selección. La misma se usa para habilitar la interfaz de comunicación del ADC.
- **DIN**: entrada serial de datos del ADC. La misma se usará para configurar el chip. Dado que el AD7705 funcionará como SPI-Slave (estando el SPI-Master en la FPGA), este puerto puede considerarse MOSI (*Master Output - Slave Input*).
- **DOUT**: salida serial de datos del ADC. Haciendo un análisis similar al caso del puerto DIN, es posible considerar este como el pin MISO (*Master Input - Slave Output*).
- **RESET**: entrada (negada) de reset.
- **DRDY**: salida (negada) que indica la existencia de un nuevo dato listo para ser leído.

#### **Modo de funcionamiento**

El AD7705 cuenta con dos entradas diferenciales de ganancia programable. Los posibles valores de ganancia son: 1, 2, 4, 8, 16, 32, 64 y 128. Esto permite medir, para una tensión de referencia **VREF=2.5V**, rangos de señales tan pequeños como 0mV - 20 mV en modo unipolar o +/- 20mV en modo bipolar. También admite rangos de mayor nivel como 0V - 2.5V en modo unipolar o +/- 2.5V en modo bipolar.

Para el modo de trabajo **unipolar**, el código resultante de las conversiones queda definido por la expresión:

$$
code = (AIN.GAIN.2^{16})/VREF
$$

Dado que es posible adquirir este ADC en formato de "placa de evaluación" (ver imagen siguiente), hay varios factores que estarán cubiertos en ella:

<div style="text-align: center;">
    <img src=".images/ad7705_pcb.png" alt="ad7705_pcb" class="center" width="500"/>
 </div>

- La tensión de referencia VREF se regula a 2.5V mediante un circuito integrado en la placa. Esto es así tanto para tensiones de alimentación de 5V como 3.3V. 
- Las entradas analógicas AIN1(-) y AIN2(-) están conectadas a GND a través de un resistor de 0 Ohm.
- Las entradas analógicas AIN1(+) y AIN2(+) se conectan al ADC a través de un divisor resistivo que reduce la tensión a la mitad. Esto, junto a la VREF de 2.5V, permitirá medir señales unipolares de 0V a 5V (tener en cuenta que la presencia del divisor afectará la expresión del código de salida del ADC vista anteriormente).
- Utiliza un cristal de 4.9152 MHz montado en la placa, el cuál será dividido por 2 internamente para generar el clock de trabajo de 2.4576 MHz.

La razón a la cual se actualizan los datos a la salida del ADC (*output rate*) es configurable, pudiendo obtenerse los siguientes valores:

![output_rates](.images/output_rates.png)

En este caso de aplicación, dado que la placa de evaluación utiliza un cristal de 4.9152 MHz, los valores posibles de output rate que se podrán obtener son aquellos en la mitad inferior de la tabla presentada. Esto se explicará con mayor detalle en las próximas secciones.

#### **Registros internos del AD7705**

El AD7705 contiene un banco de registros que permitirán configurar el chip y leer las muestras digitalizadas mediante accesos de escritura y lectura a través de su **interfaz SPI**. 

Los registros indicados en su hoja de datos son 7 y se listan a continuación:

- **Communication register**
- **Setup register**
- **Clock register**
- **Data register**
- **Test register**
- **Zero-scale calibration register**
- **Full-scale calibration register**

Los registros utilizados para la aplicación propuesta se explicarán con mayor detalle en los siguientes párrafos.

**Registro de comunicación**

Es un registro de 8 bits del tipo R/W (lectura / escritura) que controla el acceso al resto de los registros del chip. Cada acceso, ya sea de lectura o escritura, debe comenzar mediante una escritura al registro de comunicación, ya que en él se indica:

- Qué registro será accedido en la próxima operación.
- Qué tipo de operación será la siguiente (lectura o escritura).
- Qué canal del ADC estará relacionado a la operación siguiente.

Para mapear los registros internos, el AD7705 asigna una dirección a cada uno de ellos (incluido al registro de comunicación). Esta asignación se muestra en la siguiente tabla:

|    Registro                    |   Dirección   |
|--------------------------------|---------------|
|Communication register          |      000      |
|Setup register                  |      001      |
|Clock register                  |      010      |
|Data register                   |      011      |
|Test register                   |      100      |
|Zero-scale calibration register |      110      |
|Full-scale calibration register |      111      |

La siguiente imagen (tomada de la hoja de datos del ADC) explica con mayor detalle el propósito de cada bit del registro de comunicaciones y su funcionamiento:

![communication_reg](.images/communication_reg.png)

**Registro de Setup**

Es un registro de 8 bits de lectura/escritura y su configuración determina:

- El modo de operación.
- La ganancia.
- Selección de conversiones unipolar / bipolar.
- Control de buffer para la entrada analógica.
- Sincronización de filtro.

![setup_reg](.images/setup_register.png)

**Registro de Clock**

Es un registro de 8 bits de lectura/escritura que controla:

- Habilitación del Master Clock interno del chip.
- Selección de divisor de clock de entrada.
- Selección de *Output Update Rate*, es decir la frecuencia a la que se entregan nuevas muestras a la salida.

![clock_reg](.images/clock_register.png)

**Registro de Datos**

Es un registro de 16 bits de solo lectura que almacena el resultado de la última conversión del ADC.


### Diseño propuesto

Aprovechando la existencia del periférico SPI [presentado anteriormente](../23-SPI/), el enfoque propuesto para este caso es utilizar una máquina de estados finitos (FSM) para comunicarse con el AD7705, configurar sus registros y leer las muestras que entrega. 

Haciendo memoria sobre lo explicado en el ejemplo del SPI, es posible recordar que el periférico en sí fue implementado **también** como una FSM. 

La diferencia que hay entre el caso del periférico SPI y este caso de aplicación es que el primero implementa un bloque **SPI Master genérico** para propósito general; mientras que el ejemplo actual usará ese periférico SPI para una **aplicación particular** (leer muestras del AD7705). 

A modo de conclusión, el caso que se verá aquí es el de "una FSM que controla a otra FSM". En otras palabras, habrá una FSM en el periférico SPI y otra FSM "por encima" que utilizará a la anterior para un fin específico.

#### Diagrama en bloques:

![ad7705_ctrl_bd](.images/ad7705_ctrl_bd.png)

## 2. Código

Esta sección explicará los componentes principales del top level: [ad7705_ctrl.v](./Verilog/ad7705_ctrl.v)

### Interfaz

A continuación se presenta el listado de puertos de entrada y salida propuestos para este ejemplo:

```verilog
module ad7705_ctrl
(
    input             clk_in       ,
    input             rst_in_n     ,
    input             mux_sel_in_n ,
    input             drdy_in_n    ,
    input             MISO_in      ,
    output            MOSI_out     ,
    output            cs_out       ,
    output            sclk_out     ,
    output reg        dready_out   ,
    output            rst_out_n    ,
    output reg [15:0] data_out     ,
    output reg [3:0]  leds_out      
);

```

- **clk_in**: entrada de clock de la FPGA.
- **rst_in_n**: reset general del sistema (activo en bajo).
- **mux_sel_in_n**: entrada de selección para multiplexor de salida de LEDs. Esto permitirá conectar señales internas de interés a los LEDs de la EDU-CIAA-FPGA.
- **drdy_in_n**: entrada (negada) de datos disponibles, proveniente del ADC.
- **MISO_in**: entrada serial de datos (SPI), proveniente del ADC.
- **MOSI_out**: salida serial de datos (SPI), conectada a la entrada correspondiente del ADC.
- **cs_out**: salida de selección (SPI), conectada a la entrada correspondiente del ADC.
- **sclk_out**: salida de clock (SPI), conectada a la entrada correspondiente del ADC.
- **dready_out**: salida indicadora de datos disponibles.
- **rst_out_n**: salida de reset (negado), conectada a la entrada correspondiente del ADC.
- **data_out**: salida de 16 bits de datos en paralelo.
- **leds_out**: salida de LEDs de la EDU-CIAA-FPGA. **Aquí se conectarán los 4 bits más significativos del último dato leído del ADC.**

### Parámetros del AD7705

Muchos de los parámetros de configuración del AD7705 se definirán fuera del top level, en un archivo. Ese archivo es [ad7705_params.v](./Verilog/ad7705_params.v). El mismo incluye:

- Direcciones de los registros del ADC.
- Modo de operación SPI (CPOL, CPHA).
- Valores posibles de ganancia.
- Valores posibles para *output sample rate*.
- Otros valores de configuración.

A modo de ejemplo, el siguiente código muestra cómo se definen las direcciones de los registros del ADC:

```verilog
//Registros internos
`define COMM_REG   3'b000
`define SETUP_REG  3'b001
`define CLOCK_REG  3'b010
`define DATA_REG   3'b011
`define TEST_REG   3'b100
`define ZSC_REG    3'b110
`define FSC_REG    3'b111
```

Este archivo será luego incluido en el top level de la siguiente forma:

```verilog
`include "ad7705_params.v"
```

### Generación de clocks

En todo diseño digital, una de las tareas principales es definir la generación del clock (o clocks si hay más de uno). En este caso particular, si bien se mencionó el cristal de cuarzo presente en la placa de evaluación del AD7705, no hay que olvidar el puerto **SCLK**.

El clock **SCLK** será utilizado para sincronizar la interfaz de comunicación SPI del ADC. Siendo este un puerto de entrada del AD7705, es posible concluir que dicho clock tendrá origen en la **FPGA**.

La EDU-CIAA-FPGA cuenta con un oscilador de **12 MHz** montado en el PCB y conectado al pin 94 de la iCE40. Aquí es posible preguntarse: ¿Por qué no utilizar el clock de 12 MHz para sincronizar la interfaz SPI del ADC? Para responder esta pregunta, será necesario investigar **cuál es la máxima frecuencia admisible de SCLK** en la hoja de datos del AD7705:

![sclk_max_freq](.images/sclk_max_freq.png)

Como se observa en la imagen anterior, la duración mínima de los estados HIGH y LOW de SCLK (tanto para lectura como escritura) es de **100 ns**. Esto da como resultado, sumando la duración de ambos pulsos, el **período mínimo de SCLK**:

$$
T_{min}=T_{HIGH}+T_{LOW}=200ns
$$

De lo anterior se deduce que la **frecuencia máxima** de SCLK es:

$$
f_{MAX}=1/200ns=5MHz
$$

El cálculo anterior permite responder la pregunta planteada: no es posible utilizar el clock de la EDU-FPGA para sincronizar la interfaz SPI del ADC. **Sin embargo**, es posible generar un clock de menor frecuencia a partir de los 12 MHz.

Partiendo del análisis anterior, se implementará un **divisor de clock** con un simple contador de 4 bits. De esta forma, el clock de 12 MHz será dividido por 4, obteniendo así un clock de 3 MHz, el cual se encuentra dentro de la frecuencia de trabajo admitida por el SPI del ADC.

Dado que no existe un requerimiento particular sobre la frecuencia a la que debe trabajar internamente el circuito, es posible utilizar este clock de 3 MHz para sincronizar también la FSM interna. De esta forma, tanto el SPI como la FSM y la lógica de salida trabajarán a una frecuencia de 3 MHz. Este clock único será llamado **glbl_clk**.

Para optimizar la *performance* del circuito, en particular la distribución del clock, se utilizará uno de los **Global Buffer** de la iCE40. El propósito del Global Buffer es aumentar el *fanout* de una señal interna para poder ser ruteada y conectada a varias entradas. Las señales de clock son un típico ejemplo de este tipo de señales, ya que en este caso la salida del contador será conectada a todas las entradas de clock de los flip-flop presentes en el circuito.

Este buffer solo será necesario en el "mundo real", es decir, una vez que el circuito se implementa **físicamente** en la FPGA. Dado que el buffer no implica un cambio de frecuencia o transformación del clock, es posible **puentearlo** o simplemente reemplazarlo por una **asignación contínua** en las simulaciones funcionales. Esto se realiza agregando la macro **SIMULATION**, la cual será definida en el testbench y permitirá compilar condicionalmente el circuito:

```verilog
//Divisor de clock
always @ (posedge clk_in, negedge rst_in_n)
begin
    if(!rst_in_n)
        div_cntr <= {NBITS_CLK_DIV{1'b0}};
    else
        div_cntr <= div_cntr+1;
end

//Buffer Global
`ifndef SIMULATION
SB_GB sbGlobalBuffer_inst0
( 
    .USER_SIGNAL_TO_GLOBAL_BUFFER (div_cntr[NBITS_CLK_DIV-1]),
    .GLOBAL_BUFFER_OUTPUT         (glbl_clk                 )  
);
`else
  assign glbl_clk = div_cntr[NBITS_CLK_DIV-1];
`endif
```

### Periférico SPI

El siguiente bloque de código muestra la instanciación del periférico SPI. Tal como se explicó previamente, los puertos **drdy_in_n*, **serialD_in**, **serialD_out**, **cs_out** y **sclk_out** serán conectados a la interfaz del ADC. Por otro lado, las señales **dataReceived_out**, **shifting_out**, **write_in**, **read_in** y **data_in** estarán conectadas a la FSM y/o a la lógica de salida, dependiendo el caso. Finalmente, la salida **data_out** se conectará directamente a la lógica de salida:

```verilog
`include "spi.v"

(...)

//Interfaz SPI
spi
#(
    .dataBits         (DATABITS  ),
    .cPol             (`CPOL     ),
    .cPha             (`CPHA     ),
    .csPol            (`CSPOL    )
)
spi_0
(
    .clk_in           (glbl_clk           ),
    .rst_in_n         (rst_in_n           ),
    .drdy_in_n        (dready_in_n_reg    ),
    .serialD_in       (MISO_in            ),
    .serialD_out      (spi_MOSI           ),
    .cs_out           (spi_cs             ),
    .sclk_out         (spi_sclk           ),
    .dataReceived_out (spi_dataReceived_s ),
    .shifting_out     (spi_shifting_s     ),
    .write_in         (spi_write_s        ),
    .read_in          (spi_read_s         ),
    .data_in          (spi_data_i         ),
    .data_out         (spi_data_o         )
);
```

Un detalle importante a tener en cuenta son los parámetros del SPI al momento de instanciarlo. El número de bits **dataBits** se define igual a 16. Esto se hace así porque los accesos de lectura y escritura a los registros de configuración principales (SETUP y CLOCK) requieren una escritura previa al registro de comunicación del ADC. Lo anterior da un total de 8 + 8 bits transmitidos por cada operación de lectura o escritura a esos registros. El caso del registro de datos es distinto pero queda contemplado en esta configuración, ya que tiene un tamaño de 16 bits.

El modo de funcionamiento del SPI queda definido por los parámetros **cPol**, **cPha** y **csPol**. Estos dependen del AD7705 y se definen en [ad7705_params.v](./Verilog/ad7705_params.v).

### FSM

El diagrama de estados y transiciones propuesto y la tabla con las salidas de la FSM se presentan a continuación:

![fsm_diagram](.images/fsm_diagram.png)



|    Estado         |   spi_write_s |   spi_read_s  |    spi_data_i[15:0] |
|-------------------|---------------|---------------|---------------------|
|IDLE               |       0       |       0       |        0            |
|CLK_CFG            |       1       |       0       | `CLK_CONFIGURATION  |
|SETUP_CFG          |       1       |       0       |`SETUP_CONFIGURATION |
|WAIT_FOR_SAMPLE    |       0       |       0       |        0            |
|SET_CFG_TO_READ    |       1       |       0       |  `CFG_READ_DATA     |
|READ_SAMPLE        |       0       |       1       |        0            |
|WAIT_ON_CLK_CFG    |       0       |       0       |        0            |
|WAIT_ON_SETUP_CFG  |       0       |       0       |        0            |

Las salidas de la FSM son 3 y se detallan a continuación:

1. **spi_write_s**: señal que habilita la escritura por SPI.
2. **spi_read_s**: señal que habilita la lectura por SPI.
3. **spi_data_i[15:0]**: entrada paralelo de datos del periférico SPI.

Del diagrama presentado puede verse que los primeros 5 estados componen la fase de configuración del ADC. En ella se escriben los registros de SETUP y CLOCK. Luego, la FSM itera sobre los últimos 3 estados, leyendo las muestras del registro de datos del ADC.

La configuración relacionada al clock y setup está definida como una concatenación. Los 8 bits más significativos de esta concatenación representan la escritura al registro de comunicación, donde se indica qué tipo de operación será la siguiente y a qué canal y registro del ADC estará destinada. Los 8 bits menos significativos representan el valor que será escrito en el registro indicado (CLOCK o SETUP en este caso):

```verilog
`define CLK_CONFIGURATION    {1'b0, `CLOCK_REG, `WRITE, 1'b0, `CHANNEL_0, 4'b0000, `CLKDIV, `O_RATE_50HZ} // 0x200C
`define SETUP_CONFIGURATION  {1'b0, `SETUP_REG, `WRITE, 1'b0, `CHANNEL_0, `SELF_CALIBRATION_MODE, `GAIN_X1, `UNIPOLAR_OPERATION, `BUFFER_SHORTED_OUT, `FSYNC_LOW} //0x1044
```

- **CLKDIV**: es seteado en 1, dado que la placa del AD7705 utiliza un cristal de 4.9152 MHz, el cual debe ser dividido por 2 internamente según la hoja de datos del ADC.
- **CLK, FS1, FS0**: quedan definidos por la macro **O_RATE_50HZ**, la cual se encuentra en [ad7705_params.v](./Verilog/ad7705_params.v). Esto resultará en una muestra cada 20 ms (50 Hz) disponible a la salida del ADC.
- **Ganancia**: queda definida por la macro **GAIN_X1**, es decir ganancia igual a 1.
- **Operación unipolar**: se elige este modo de trabajo porque, como se explicó previamente, las entradas AIN(-) de la placa de evaluación del AD7705 están conectadas a GND, por lo tanto la medición será del tipo unipolar. 
- **FSYNC_LOW**: el bit FSYNC del registro de setup se pone a cero para que el modulador del ADC procese la entrada analógica para generar muestras digitales.

Para la lectura de datos, el procedimiento es el siguiente:

1. La FSM espera en el estado **WAIT_FOR_SAMPLE** hasta recibir un flanco descendente en el registro **dready_in_n_reg**, el cual registra el pin de entrada **drdy_in_n** proveniente del ADC.
2. Se indica en el registro de comunicaciones que la próxima operación será **una escritura a sí  mismo**. Este punto y el siguiente están representados por el estado **SET_CFG_TO_READ**
3. Se escribe en el registro de comunicaciones la dirección del registro de datos, indicando que la operación siguiente será una **lectura** desde ese registro.
4. Por último, en el estado **READ_SAMPLE** se hace una lectura de 16 bits, los cuales tendrán el resultado de la última conversión.
5. Al finalizar la lectura, el registro de salida se actualizará con la muestra de 16 bits leída.

Los puntos **2 y 3** del procedimiento anterior se definen mediante la siguiente concatenación:

```verilog
`define CFG_READ_DATA        {1'b0, `COMM_REG , `WRITE, 1'b0, `CHANNEL_0, 1'b0, `DATA_REG , `READ, 1'b0, `CHANNEL_0} //0X0038
```

El siguiente diagrama de la **hoja de datos** muestra una secuencia de **configuración de referencia**:

![configuration_procedure](.images/configuration_procedure.png)

#### Parte secuencial de la FSM:

```verilog
//Parte secuencial de la FSM
always @ (posedge glbl_clk, negedge rst_in_n)
begin
    if(!rst_in_n)
        currentState_s <= IDLE;
    else 
        currentState_s <= nextState_s;
end
```

#### Parte combinacional de la FSM:

Esta sección muestra la lógica combinacional relacionada a la FSM, la cual implica la actualización del estado futuro y la lógica de salida. Puede observarse que las salidas dependen únicamente del estado actual, por lo tanto es una FSM tipo ***Moore***:

```verilog
//Parte combinacional de la FSM
always @(*)
begin
    case(currentState_s)
        IDLE:
        begin
            nextState_s    = CLK_CFG          ;
            spi_write_s    = 1'b0             ;
            spi_read_s     = 1'b0             ;
            spi_data_i     = {DATABITS{1'b0}} ;
        end
        
        CLK_CFG:
        begin
            if(spi_shifting_s)
                nextState_s = WAIT_ON_CLK_CFG;
            else
                nextState_s = CLK_CFG;
            spi_write_s    = 1'b1                 ;
            spi_read_s     = 1'b0                 ;
            spi_data_i     = `CLK_CONFIGURATION   ;
        end

        SETUP_CFG:
        begin
            if(spi_shifting_s)
                nextState_s = WAIT_ON_SETUP_CFG;
            else
                nextState_s = SETUP_CFG;
            spi_write_s    = 1'b1                 ;
            spi_read_s     = 1'b0                 ;
            spi_data_i     = `SETUP_CONFIGURATION ;
        end
    
        WAIT_FOR_SAMPLE:
        begin
            if(!dready_in_n_reg)
                nextState_s = SET_CFG_TO_READ;
            else
                nextState_s = WAIT_FOR_SAMPLE;
            spi_write_s    = 1'b0             ;
            spi_read_s     = 1'b0             ;
            spi_data_i     = {DATABITS{1'b0}} ;
        end

        SET_CFG_TO_READ:
        begin
            if(spi_dataReceived_s)
                nextState_s = READ_SAMPLE;
            else
                nextState_s = SET_CFG_TO_READ;
            spi_write_s    = 1'b1            ;
            spi_read_s     = 1'b0            ;
            spi_data_i     = `CFG_READ_DATA  ;
        end
    
        READ_SAMPLE:
        begin
            if(spi_dataReceived_s)
                nextState_s = WAIT_FOR_SAMPLE;
            else
                nextState_s = READ_SAMPLE;
            spi_write_s    = 1'b0             ;
            spi_read_s     = 1'b1             ;
            spi_data_i     = {DATABITS{1'b0}} ;
        end 
        
        WAIT_ON_CLK_CFG:
        begin
            if(spi_dataReceived_s)
                nextState_s = SETUP_CFG;
            else
                nextState_s = WAIT_ON_CLK_CFG;
            spi_write_s    = 1'b0                 ;
            spi_read_s     = 1'b0                 ;
            spi_data_i     = {DATABITS{1'b0}}     ;
        end 

        WAIT_ON_SETUP_CFG:
        begin
            if(spi_dataReceived_s)
                nextState_s = WAIT_FOR_SAMPLE;
            else
                nextState_s = WAIT_ON_SETUP_CFG;
            spi_write_s    = 1'b0                 ;
            spi_read_s     = 1'b0                 ;
            spi_data_i     = {DATABITS{1'b0}}     ;
        end 

        default:
        begin
            nextState_s    = IDLE             ;
            spi_write_s    = 1'b0             ;
            spi_read_s     = 1'b0             ;
            spi_data_i     = {DATABITS{1'b0}} ;
        end
    endcase;
end
```

### Salidas

La lógica de salida está compuesta por:

- Registro de salida de datos: **data_out**.
- Registro de salida de LEDs: **leds_out**. Aquí se conectarán los 4 bits más significativos del último dato leído del ADC.
- Registro de salida indicador de datos disponibles: **dready_out**.
- Salida de reset para el ADC: **rst_n_out**.
- Salidas del bus SPI: **MOSI_out**, **cs_out**, **sclk_out**.

```verilog
//Salidas
always @ (posedge glbl_clk, negedge rst_in_n)
begin
    if(!rst_in_n)
    begin
        data_out <= {DATABITS{1'b0}};
    end
    else if(spi_read_s && spi_dataReceived_s)
    begin
        data_out <= spi_data_o;
    end
end

always @ (posedge glbl_clk, negedge rst_in_n)
begin
    if(!rst_in_n)
        leds_out <= 4'b0000;
    else if(mux_sel_in_n)
        leds_out <= spi_data_o[DATABITS-1-:4];
    else
        leds_out <= spi_data_o[DATABITS-1-4-:4];
end

always @ (posedge glbl_clk, negedge rst_in_n)
begin
    if(!rst_in_n)
        dready_out <= 1'b0;
    else 
        dready_out <= (spi_read_s & spi_dataReceived_s);
end

assign rst_out_n   = rst_in_n   ;
assign MOSI_out    = spi_MOSI   ;
assign cs_out      = spi_cs     ;
assign sclk_out    = spi_sclk   ;
```

## 3. Simulación

Para verificar (por inspección visual) el comportamiento del controlador se incluye el test [ad7705_ctrl_tb.v](./Verilog/ad7705_ctrl_tb.v). La descripción básica del test es la siguiente:

1. Se instancia el DUT (*Device Under Test*) y se aplica un reset.
2. Se espera una cantidad de 6000 unidades de tiempo durante la cual el DUT enviará los comandos de configuración. Esto abarca los primeros cinco estados de la FSM. Luego de esto, el DUT quedará esperando la indicación de datos disponibles.
3. Se indica mediante la entrada **drdy_in_n** que hay datos disponibles, lo cuál hará avanzar la FSM hacia los estados restantes hasta obtener un dato a la salida. El dato será 0xFFFF, dado que la linea MISO se mantiene en '1' durante todo el test:

![tb_waveforms](.images/tb_waveforms.png)

## 4. Síntesis e implementación

### Constraints

Tanto los constraints de entrada/salida como las **definiciones de clocks** se encuentran en el archivo [ad7705_ctrl.pcf](./Verilog/ad7705_ctrl.pcf):

```
#Clocks
set_frequency clk_in 12
set_frequency glbl_clk 3

#I/O
set_io leds_out[0] 1
set_io leds_out[1] 2
set_io leds_out[2] 3
set_io leds_out[3] 4

set_io clk_in 94
set_io rst_in_n 31
set_io mux_sel_in_n 34
set_io drdy_in_n 7
set_io MISO_in 8
set_io MOSI_out 9
set_io cs_out 10
set_io sclk_out 11
set_io dready_out 12
set_io rst_out_n 15

set_io data_out[0] 107
set_io data_out[1] 106
set_io data_out[2] 105
set_io data_out[3] 104
set_io data_out[4] 99
set_io data_out[5] 98
set_io data_out[6] 97
set_io data_out[7] 96
set_io data_out[8] 95
set_io data_out[9] 85
set_io data_out[10] 84
set_io data_out[11] 83
set_io data_out[12] 82
set_io data_out[13] 81
set_io data_out[14] 80
set_io data_out[15] 79
```

>Nota: observar que la frecuencia de trabajo de los clocks se indica en unidades de MHz.

### Reportes

Al realizar la síntesis utilizando la [extensión de la EDU-CIAA-FPGA](https://gitlab.com/educiaafpga/edu-ciaa-fpga-vscode-extension) desde Vscode, el reporte de síntesis quedará guardado en **vscode_stdout.log**, mientras que el reporte de Place and Route quedará guardado en **vscode_stderr.log**.

#### Reporte de síntesis.

```
=== ad7705_ctrl ===

   Number of wires:                127
   Number of wire bits:            378
   Number of public wires:         127
   Number of public wire bits:     378
   Number of memories:               0
   Number of memory bits:            0
   Number of processes:              0
   Number of cells:                206
     SB_CARRY                       16
     SB_DFFER                       70
     SB_DFFNR                        1
     SB_DFFR                        15
     SB_DFFS                         1
     SB_DFFSR                        5
     SB_GB                           1
     SB_LUT4                        97
```

#### Reporte de PnR (Plance and Route).

```
Info: Max frequency for clock                 'glbl_clk': 169.92 MHz (PASS at 3.00 MHz)
Info: Max frequency for clock 'clk_in$SB_IO_IN_$glb_clk': 655.31 MHz (PASS at 12.00 MHz)

Info: Max delay <async>                          -> <async>                         : 1.92 ns
Info: Max delay <async>                          -> posedge clk_in$SB_IO_IN_$glb_clk: 3.75 ns
Info: Max delay <async>                          -> posedge glbl_clk                : 3.90 ns
Info: Max delay <async>                          -> negedge glbl_clk                : 3.75 ns
Info: Max delay posedge clk_in$SB_IO_IN_$glb_clk -> <async>                         : 4.68 ns
Info: Max delay posedge glbl_clk                 -> <async>                         : 5.18 ns
Info: Max delay negedge glbl_clk                 -> <async>                         : 3.89 ns
```

## 5. Prueba en EDU-CIAA-FPGA

### Diagrama de conexiones

Para probar el controlador será necesario contar con:

- Potenciómetro 10k Ohm.
- Cables tipo Arduino.
- Protoboard o placa equivalente para realizar las conexiones.

La siguiente imagen muestra el esquema de conexiones para el potenciómetro y la equivalencia entre los pines del AD7705 y los puertos definidos en [ad7705_ctrl.v](./Verilog/ad7705_ctrl.v) y [ad7705_ctrl.pcf](./Verilog/ad7705_ctrl.pcf):

![ad7705_connections.png](.images/ad7705_connections.png)

>Nota: para conectar el AD7705 con los puertos de la FPGA referirse al [diagrama de conexiones](https://github.com/ciaa/Hardware/blob/master/PCB/EDU-FPGA/Pinout/Pinout%20EDU%20FPGA.pdf) de la EDU-FPGA.

### Capturando señales con analizador lógico

Luego de realizar las conexiones y bajar el diseño a la EDU-FPGA, se puede utilizar un [analizador lógico](https://www.uctronics.com/download/Amazon/U6041.pdf) para conectarse al puerto SPI y registrar las lecturas y escrituras entre la FPGA y el ADC. En este caso, se utilizó un analizador de 8 canales y 24 MHz como el que se muestra a continuación:

![saleae_la.jpg](.images/saleae_la.jpg)

Con este analizador podrán registrarse las señales SCLK, CS, MISO y MOSI. La siguiente imagen muestra la fase de configuración (primeros estados de la FSM) donde se envían los comandos 0x200C (configuración del registro de clock) y 0x1044 (configuración del registro de setup):

![la_initial_config](.images/la_initial_config.png)

Luego, la FSM avanza con la lectura de datos. En este caso, pueden registrarse los comandos de lectura 0x0038 junto a los datos leídos. En la captura siguiente se observa que la muestra leída por la línea MISO tiene el valor 0x5B1A:

![la_data_read](.images/la_data_read.png)

## 6. Conclusiones

El ejemplo presentado es una demostración que abarca el uso de un *driver* SPI genérico para leer datos de un ADC. La filosofía propuesta busca resolver el problema utilizando máquinas de estados finitos, las cuales son empleadas en una gran variedad de aplicaciones tanto simples como avanzadas. De esta forma, el lector o lectora podrán revisar los criterios de diseño seguidos en este tutorial para resolver problemas similares que puedan surgir en el mundo digital.

## 7. Información adicional: teoría y uso de herramientas

- [Wiki del proyecto EDU-CIAA-FPGA](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/-/wikis/Tabla-de-Contenidos)

- [Herramientas de desarrollo de la EDU-CIAA-FPGA](https://gitlab.com/educiaafpga/herramientas)

- [Lattice iCE40 FPGAs](https://www.latticesemi.com/iCE40)

- [Lattice iCE40 Technology Library](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjN5N6F4a2BAxWIq5UCHXD2BuQQFnoECBYQAQ&url=https%3A%2F%2Fwww.latticesemi.com%2F-%2Fmedia%2FLatticeSemi%2FDocuments%2FTechnicalBriefs%2FFPGA-TN-02026-3-2-iCE40-Technology-Library.ashx%3Fdocument_id%3D52206&usg=AOvVaw3CObOIydvCF3HY_nM4Hi7v&opi=89978449)
