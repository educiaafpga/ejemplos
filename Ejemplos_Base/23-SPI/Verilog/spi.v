/*------------------------------------------------------------------------
---- Module:  spi                                                     ----
---- Description: SPI Master core configurable in 4 operating modes   ----
---- Author/s:  Martín A. Heredia                                     ----
---- Last revision: 07/04/2023                                        ----
---- Dependencies:                                                    ----
----                                                                  ----
------------------------------------------------------------------------*/

module spi
#(
    parameter dataBits = 4,
    parameter cPol     = 1'b1,
    parameter cPha     = 1'b1,
    parameter csPol    = 1'b0
)
(
    input                 clk_in           , // Entrada de clock
    input                 rst_in_n         , // Entrada de reset negado
    input                 drdy_in_n        , // Indicación de datos disponibles para leer
    input                 serialD_in       , // MISO
    output                serialD_out      , // MOSI
    output                cs_out           , // Chip select
    output                sclk_out         , // SCLK para lectura/escritura
    output                dataReceived_out , // Indicación de datos recibidos
    output                shifting_out     , // Indicación de transmisión en curso
    input                 write_in         , // Señal para comenzar proceso de transmisión de datos (escritura)
    input                 read_in          , // Señal para habilitar lectura cuando hay datos disponibles
    input  [dataBits-1:0] data_in          , // Entrada de datos para Tx
    output [dataBits-1:0] data_out           // Datos leidos de Rx
);

//Declaración de señales internas:
//Chip Select y flancos de lectura y desplazamiento
localparam SHIFT_OUT_EDGE = cPol ^ cPha;
localparam SHIFT_IN_EDGE  = ~SHIFT_OUT_EDGE;
localparam CS_ACTIVE      = csPol;
localparam CS_INACTIVE    = ~csPol;
//SCKL
wire sclk;
reg  sclkOutput_rst_n;
wire sclkOutputSelect_s;
localparam PULSE_CNTR_N_BITS = $clog2(dataBits);
reg  [PULSE_CNTR_N_BITS-1:0] pulseCounter_cnt;
reg  pulseCounterTC_s;
//Clocks internos
wire cntr_clk;
wire shift_i_clk;
wire shift_o_clk;
//Registro de entrada
reg  [dataBits-1:0] din_reg;
reg  loadDin_ena;
//Registro de salida
reg  [dataBits-1:0] dout_reg;
reg  loadDout_ena;
//Registro de desplazamiento
reg  [dataBits-1:0] shift_reg;
reg  [dataBits-1:0] shiftBitsIn_s;
reg  shiftLoad_ena_n;
reg  shift_ena;
reg  dataRead_s;
//FSM
localparam IDLE       = 3'b000;
localparam LOAD_INPUT = 3'b001;
localparam LOAD_SR    = 3'b010;
localparam CSELECT    = 3'b011;
localparam SHIFT      = 3'b100;
localparam DONE       = 3'b101;
localparam CNOTSELECT = 3'b110;
reg  [2:0] currentState_s;
reg  [2:0] nextState_s;
reg dataReceived_s;
reg shifting_s;
reg cs_s;
reg dataReceived_reg;
reg shifting_reg;

//Registro para desplazamiento de salida con FFD
reg serialD_o_reg;

//Descripción de la lógica:

//Salidas
always @ (posedge shift_i_clk, negedge rst_in_n)
begin
    if(!rst_in_n)
    begin
        dataReceived_reg <= 1'b0;
        shifting_reg <= 1'b0;
    end
    else
    begin
        dataReceived_reg <= dataReceived_s;
        shifting_reg <= shifting_s;
    end
end
assign sclk_out         = sclk;
assign data_out         = dout_reg;
assign dataReceived_out = dataReceived_reg;
assign shifting_out     = shifting_reg;
assign cs_out           = cs_s;
assign serialD_out      = serialD_o_reg;

//SCLK
// Aquí se rutea el clock a lógica combinacional. El uso de 
// global buffers puede ser necesario para mejorar la performance.
assign sclkOutputSelect_s = sclkOutput_rst_n & (~pulseCounterTC_s);
assign sclk = (sclkOutputSelect_s) ? (clk_in) : (cPol);

//Generación de clocks internos
generate
    if(SHIFT_IN_EDGE)
    begin
        assign shift_i_clk = clk_in;
        assign shift_o_clk = ~clk_in;
    end
    else
    begin
        assign shift_i_clk = ~clk_in;
        assign shift_o_clk = clk_in;
    end
endgenerate

generate
    if(cPol)
        assign cntr_clk = clk_in;
    else
        assign cntr_clk = ~clk_in;
endgenerate

//Contador de pulsos de clock en función del modo SPI
always @ (posedge cntr_clk)
begin
    if(!sclkOutput_rst_n)
    begin
        pulseCounter_cnt <= {PULSE_CNTR_N_BITS{1'b0}};
        pulseCounterTC_s <= 1'b0;
    end
    else
    begin
        if(pulseCounter_cnt == (dataBits-1))
        begin
            pulseCounter_cnt <= {PULSE_CNTR_N_BITS{1'b0}};
            pulseCounterTC_s <= 1'b1;
        end
        else
        begin
           pulseCounter_cnt <= pulseCounter_cnt + 1;
           pulseCounterTC_s <= 1'b0;
        end
    end
end

//Registro de desplazamiento
always @ (posedge shift_i_clk, negedge rst_in_n)
begin
    if(!rst_in_n)
    begin
        shift_reg     <= {dataBits{1'b0}};
        shiftBitsIn_s <= {dataBits{1'b0}};
        dataRead_s    <= 1'b0;
    end
    else
    begin
        if(!shiftLoad_ena_n)
        begin
            shift_reg     <= din_reg;
            shiftBitsIn_s <= {dataBits{1'b0}};
            dataRead_s    <= 1'b0;
        end
        else if(shift_ena == 1'b1)
        begin
            shift_reg <= {shift_reg[dataBits-2:0], serialD_in};
	        if(shiftBitsIn_s == (dataBits-2))
            begin
	            dataRead_s    <= 1'b1;
                shiftBitsIn_s <= shiftBitsIn_s+1;
            end
            else if(shiftBitsIn_s == (dataBits-1))
            begin
                dataRead_s    <= 1'b0;
                shiftBitsIn_s <= {dataBits{1'b0}};
            end
            else
            begin
	            dataRead_s    <= 1'b0;
                shiftBitsIn_s <= shiftBitsIn_s+1;
	        end
        end
    end
end

//Registro de entrada
always @ (posedge shift_i_clk, negedge rst_in_n)
begin
    if(!rst_in_n)
        din_reg <= {dataBits{1'b0}};
    else if(loadDin_ena == 1'b1)
        din_reg <= data_in;
end

//Registro de salida
always @ (posedge shift_i_clk, negedge rst_in_n)
begin
    if(!rst_in_n)
        dout_reg <= {dataBits{1'b0}};
    else if(loadDout_ena == 1'b1)
        dout_reg <= shift_reg;
end

//Desplazamiento de salida con FF D
always @ (posedge shift_o_clk, negedge rst_in_n)
begin
    if(!rst_in_n)
        serialD_o_reg <= 1'b0;
    else 
        serialD_o_reg <= shift_reg[dataBits-1];
end

//Parte secuencial de la FSM
always @ (posedge shift_i_clk, negedge rst_in_n)
begin
    if(!rst_in_n)
        currentState_s <= IDLE;
    else 
        currentState_s <= nextState_s;
end

//Parte combinacional de la FSM
always @(*)
begin
    case(currentState_s)
        IDLE:
        begin
            if(write_in)
                nextState_s = LOAD_INPUT;
            else if(!drdy_in_n && read_in)
                nextState_s = CSELECT;
            else
                nextState_s = IDLE;
            //Señales de control
            shiftLoad_ena_n  = 1'b1;
            shift_ena        = 1'b0;
            sclkOutput_rst_n = 1'b0;
            loadDin_ena      = 1'b0;
            loadDout_ena     = 1'b0;
            //Salidas
            dataReceived_s   = 1'b0;
            cs_s             = CS_INACTIVE;
            shifting_s       = 1'b0;
        end
        
        LOAD_INPUT:
        begin
            nextState_s = LOAD_SR;
            //Señales de control
            shiftLoad_ena_n  = 1'b1;
            shift_ena        = 1'b0;
            sclkOutput_rst_n = 1'b0;
            loadDin_ena      = 1'b1;
            loadDout_ena     = 1'b0;
            //Salidas
            dataReceived_s   = 1'b0;
            cs_s             = CS_INACTIVE;
            shifting_s       = 1'b0;
        end

        LOAD_SR:
        begin
            nextState_s = CSELECT;
            //Señales de control
            shiftLoad_ena_n  = 1'b0;
            shift_ena        = 1'b0;
            sclkOutput_rst_n = 1'b0;
            loadDin_ena      = 1'b0;
            loadDout_ena     = 1'b0;
            //Salidas
            dataReceived_s   = 1'b0;
            cs_s             = CS_INACTIVE;
            shifting_s       = 1'b0;
        end
    
        CSELECT:
        begin
            nextState_s = SHIFT;
            //Señales de control
            /*En caso de realizar la lectura de datos
            con el primer flanco de sclk_out (cPha = '0'),
            el registro de desplazamiento debe habilitarse
            un ciclo de clock antes del estado SHIFT. De lo contrario,
            si cPha = '1', el registro de desplazamiento se habilita
            en el estado SHIFT*/
            shiftLoad_ena_n  = 1'b1;
            shift_ena        = ~cPha; 
            sclkOutput_rst_n = 1'b0;
            loadDin_ena      = 1'b0;
            loadDout_ena     = 1'b0;
            //Salidas
            dataReceived_s   = 1'b0;
            cs_s             = CS_ACTIVE;
            shifting_s       = 1'b0;
        end

        SHIFT:
        begin
            if(dataRead_s)
                nextState_s = DONE;
            else
                nextState_s = SHIFT;
            //Señales de control
            shiftLoad_ena_n  = 1'b1;
            shift_ena        = 1'b1;
            sclkOutput_rst_n = 1'b1;
            loadDin_ena      = 1'b0;
            loadDout_ena     = 1'b0;
            //Salidas
            dataReceived_s   = 1'b0;
            cs_s             = CS_ACTIVE;
            shifting_s       = 1'b1;
        end

        DONE:
        begin
            nextState_s = CNOTSELECT;
            //Señales de control
            /*En los casos donde cPha = '1', el último
            flanco de clock se utiliza para leer datos,
            de esta forma, sclk_out vuelve al estado idle
            luego de la última lectura. Por el contrario,
            cuando el modo de funcionamiento está definido con
            cPha = '0', el último flanco del sclk_out se usa para
            desplazar el próximo bit de datos (tanto en Master
            como en Slave), es decir que luego de leer el último
            bit, es necesario un flanco adicional en sclk_out para
            realizar esto último.*/
            shiftLoad_ena_n  = 1'b1;
            shift_ena        = 1'b0;
            sclkOutput_rst_n = ~cPha; 
            loadDin_ena      = 1'b0;
            loadDout_ena     = 1'b1;
            //Salidas
            dataReceived_s   = 1'b0;
            cs_s             = CS_ACTIVE;
            shifting_s       = 1'b0;
        end
    
        CNOTSELECT:
        begin
            nextState_s = IDLE;
            //Señales de control
            shiftLoad_ena_n  = 1'b1;
            shift_ena        = 1'b0;
            sclkOutput_rst_n = 1'b0;
            loadDin_ena      = 1'b0;
            loadDout_ena     = 1'b0;
            //Salidas
            dataReceived_s   = 1'b1;
            cs_s             = CS_INACTIVE;
            shifting_s       = 1'b0;
        end 
        
        default:
        begin
            nextState_s = IDLE;
            //Señales de control
            shiftLoad_ena_n  = 1'b1;
            shift_ena        = 1'b0;
            sclkOutput_rst_n = 1'b0;
            loadDin_ena      = 1'b0;
            loadDout_ena     = 1'b0;
            //Salidas
            dataReceived_s   = 1'b0;
            cs_s             = CS_INACTIVE;
            shifting_s       = 1'b0;
        end
    endcase;
end
endmodule
