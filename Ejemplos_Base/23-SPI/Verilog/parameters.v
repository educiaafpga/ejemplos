
`ifndef _spi_parameters_
`define _spi_parameters_

`define DATABITS 8
`define CPOL     1'b0
`define CPHA     1'b0
`define CSPOL    1'b0
`define PER2     (40.0/2)

`endif
