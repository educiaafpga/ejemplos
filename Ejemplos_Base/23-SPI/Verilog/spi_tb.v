/*------------------------------------------------------------------------
---- Module:  spi_tb                                                  ----
---- Description: Testbench for spi IP Core                           ----
---- Author/s:  Martín A. Heredia                                     ----
---- Last revision: 08/04/2023                                        ----
---- Dependencies:                                                    ----
----     parameters.v                                                 ----
------------------------------------------------------------------------*/

`timescale 1 ns/100 ps
`include "parameters.v"
`include "spi.v"

module spi_tb();

//Parámetros del test:
localparam CPOL_TEST        = `CPOL;
localparam CPHA_TEST        = `CPHA;
localparam SHIFT_OUT_EDGE   = (`CPOL ^ `CPHA);
localparam SHIFT_IN_EDGE    = ~SHIFT_OUT_EDGE;
localparam CSPOL            = `CSPOL;
localparam CS_ACTIVE        = CSPOL;
localparam CS_INACTIVE      = ~(`CSPOL);
localparam DATABITS         = `DATABITS;
localparam STIM_VEC_LENGTH  = DATABITS*(2**DATABITS);

//Señales internas:
reg                          test_clk_in;
reg                          test_rst_in_n;
reg                          test_drdy_in_n;
reg                          test_serialD_in;
wire                         test_serialD_out;
wire                         test_cs_out;
wire                         test_sclk_out;
wire                         test_dataReceived_out;
wire                         test_shifting_out;
reg                          test_write_in;
reg                          test_read_in;
reg  [DATABITS-1:0]          test_data_in;
wire [DATABITS-1:0]          test_data_out;
reg  [DATABITS-1:0]          dataToDUT_s;
reg                          dataSent_s;
reg  [DATABITS-1:0]          dataReceivedFromDUT_s;
reg  [$clog2(DATABITS)-1:0]  i;
wire                         tx_clk;
wire                         rx_clk;

//DUT:
spi
#(
    .dataBits         (DATABITS  ),
    .cPol             (CPOL_TEST ),
    .cPha             (CPHA_TEST ),
    .csPol            (CSPOL     )
)
dut
(
    .clk_in           (test_clk_in           ),
    .rst_in_n         (test_rst_in_n         ),
    .drdy_in_n        (test_drdy_in_n        ),
    .serialD_in       (test_serialD_in       ),
    .serialD_out      (test_serialD_out      ),
    .cs_out           (test_cs_out           ),
    .sclk_out         (test_sclk_out         ),
    .dataReceived_out (test_dataReceived_out ),
    .shifting_out     (test_shifting_out     ),
    .write_in         (test_write_in         ),
    .read_in          (test_read_in          ),
    .data_in          (test_data_in          ),
    .data_out         (test_data_out         )
);

//Clocks:
always
begin
    test_clk_in = 1'b1;
    #(`PER2);
    test_clk_in = 1'b0;
    #(`PER2);
end

generate
    if(SHIFT_IN_EDGE)
    begin
        assign tx_clk = ~test_sclk_out;
        assign rx_clk = test_sclk_out;
    end
    else
    begin
        assign tx_clk = test_sclk_out;
        assign rx_clk = ~test_sclk_out;
    end
endgenerate

//Datos enviados al DUT
always @ (posedge tx_clk, negedge test_rst_in_n)
begin
    if(!test_rst_in_n)
    begin
        dataSent_s      <= 1'b0;
        i               <= {{($clog2(DATABITS)-1){1'b0}}, ~CPHA_TEST[0]};
        test_serialD_in <= (dataToDUT_s[DATABITS-1] & ~(CPHA_TEST[0]));
    end
    else
    begin
        test_serialD_in <= dataToDUT_s[DATABITS-1-i];
        if(i < (DATABITS-1))
        begin
            i <= i+1;
            dataSent_s <= 1'b0;
        end
        else
        begin
            i <= 0;
            dataSent_s <= 1'b1;
        end
    end
end

//Datos recibidos desde el DUT
always @ (posedge rx_clk, negedge test_rst_in_n)
begin
    if(!test_rst_in_n)
        dataReceivedFromDUT_s <= {DATABITS{1'b0}};
    else 
        dataReceivedFromDUT_s <= {dataReceivedFromDUT_s[DATABITS-2:0], test_serialD_out};
end

//Test
integer nSamplesOut = 0;
integer nSamplesIn  = 0;
initial 
begin
    $dumpfile("wf.vcd");
    $dumpvars(0, spi_tb);
    //Inicio del Test
    $display("spi_tb start...");
    //Reset
    $display("Reset");
    test_rst_in_n  = 1'b0;
	test_drdy_in_n = 1'b1;
    test_data_in   = {DATABITS{1'b0}};
    test_write_in  = 1'b0;
    test_read_in   = 1'b1;
    dataToDUT_s    = {DATABITS{1'b0}};
	#(`PER2*20);
	test_rst_in_n = 1'b1;
	#(`PER2*4);

    /*Enviar datos al DUT y evaluar la recepción y retransmisión de los mismos
    La retransmisión se da cuando en el proceso de recibir datos, el DUT
    retransmite el ultimo byte (o bytes) recibidos. Es decir, al mismo tiempo
    que el DUT recibe el byte n, retransmite el byte n-1 */
    while(nSamplesOut < 2**DATABITS)
    begin
        test_drdy_in_n = 1'b0;
        while(dataSent_s != 1'b0)
            #(`PER2);
        while(dataSent_s != 1'b1)
            #(`PER2);
        dataToDUT_s = nSamplesOut+1;
        while(test_dataReceived_out != 1'b1)
            #(`PER2);
        if(test_data_out != nSamplesOut)
            $error("Error at RX in DUT.\n nSamplesOut = %d\n test_data_out = %d\n",nSamplesOut, test_data_out);
        if(nSamplesOut > 0)
        begin
            if(dataReceivedFromDUT_s != (nSamplesOut-1))
                $error("Error receiving data from DUT.\n n(SamplesOut-1) = %d\n dataReceivedFromDUT_s = %d \n",(nSamplesOut-1), dataReceivedFromDUT_s);
        end
        #(`PER2*8);
        test_drdy_in_n = 1'b1;
        #(`PER2*4);
        nSamplesOut = nSamplesOut+1;
    end

    /*Realizar una transmisión más para terminar de evaluar que
    el DUT envíe por test_serialD_out el último dato recibido por
    test_serialD_in */
    test_drdy_in_n = 1'b0;
    while(test_dataReceived_out != 1'b1)
            #(`PER2);
    if(dataReceivedFromDUT_s != (2**DATABITS - 1))
        $error("Error receiving data from DUT");
    #(`PER2*8);
    test_drdy_in_n = 1'b1;
    #(`PER2*4);
    test_read_in = 1'b0;
    
    //Probar el envío de datos desde el DUT hacia el Testbench
    while(nSamplesIn < 2**DATABITS)
    begin
        test_data_in = nSamplesIn;
        #(`PER2*6);
        test_write_in = 1'b1;
        #(`PER2*4);
        test_write_in = 1'b0;
        while(test_dataReceived_out != 1'b1)
            #(`PER2);
        if(dataReceivedFromDUT_s != nSamplesIn)
            $error("Error writing data out from DUT");
        nSamplesIn = nSamplesIn + 1;
    end

    //Fin del test
    $display("Test finished");
    $finish;
end

endmodule