# FPGA para Todos

[[_TOC_]]

## ¿Qué es una FPGA?
Las FPGA (Field-Programmable Gate Array) son dispositivos semiconductores digitales que pueden reconfigurarse luego de manufacturados, en contraposición a los circuitos integrados ordinarios que están limitados a una función específica impuesta por su diseño. Además, su estructura interna compuesta por bloques lógicos interconectables les provee mayor flexibilidad y poder de procesamiento que los microcontroladores o procesadores convencionales.

Esto permite que los productos implementados con FPGAs posean varias ventajas:

 - Pueden variarse sus capacidades y funcionalidad en campo.
 - Puede reutilizarse el mismo hardware variando la descripción del circuito implementado
 - Son muy apropiadas para la implementación de procesamiento en paralelo, a diferencia de los microprocesadores
 - Pueden utilizarse complementariamente con otros sistemas digitales, por ejemplo, microprocesadores ya sea en la misma pastilla o no

Algunos de los campos de aplicación de la lógica programable son, entre otros:

 - Procesamiento de Señales Digitales (DSP)
 - Radio Definida por Software (SDR)
 - Prototipado de circuitos integrados
 - Diagnostico medico
 - Instrumentación científica
 - Procesamiento de audio, vídeo e imágenes
 - Criptografia
 - Computación de alto desempeño
 - Sistemas de comunicaciones
 - Sistemas espaciales o nucleares de alta confiabilidad


## ¿Qué problemáticas existen en la enseñanza de FPGA?
Dentro de los alcances de la formación del ingeniero electrónico suele estar el aprendizaje de los lenguajes de descripción de hardware (por ejemplo, VHDL, Verilog, etc.) que se utilizan para diseñar y describir hardware digital que luego se sintetizará y se utilizará para configurar las FPGAs.

No obstante, para completar el ciclo de aprendizaje resulta necesario no sólo aprender el lenguaje a través de la implementación y simulación de circuitos, sino instanciarlos efectivamente sobre el hardware para estudiar sus implicancias prácticas.

Actualmente, la enseñanza de lógica programable constituye un área de vacancia en muchas carreras de Ingeniería Electrónica. Se trata de una temática difícil de abordar en niveles iniciales de la carrera, y más aún considerando la falta de herramientas a nivel local (kits de desarrollo a precios asequibles, material didáctico para formación inicial, ejemplos de aplicación documentados, proyectos de colaboración abierta, etc.).

## ¿Qué es el proyecto FPGA para Todos?
**FPGA para Todos** es una plataforma orientada a la divulgación y enseñanza de las FPGA y los lenguajes de descripción de hardware basada en tres pilares fundamentales:

### 1. Hardware libre y abierto a la comunidad (EDU-FPGA)
La **EDU-FPGA** es una placa diseñada por el Grupo de Investigación y Desarrollo de Sistemas Embebidos (ASE) de la UTN-FRH con el objetivo de proveer una herramienta abierta y de bajo costo con la cual cualquier persona pueda dar sus primeros pasos en el uso de FPGAs. 

### 2. Plataforma de ejemplos, documentos y herramientas libres
Además, de los recursos de hardware, el proyecto abarca una amplia collección de material didáctico, referencias y herramientas para el desarrollo de proyectos:
1. Una [Wiki](https://gitlab.com/RamadrianG/wiki---fpga-para-todos) con información detallada y ejemplos sobre lenguajes de descripción de hardware (VHDL y Verilog), conceptos básicos de diseño digital (descripción, simulación y síntesis) y uso de herramientas libres para programar la EDU-FPGA.
2. Un Repositorio de Ejemplos y proyectos de todo tipo, en constante crecimiento y con la documentación correspondiente a cada ejemplo (en preparación a liberarse en el mes de junio de 2020)

### 3. Red nacional de colaboradores, revisores y usuarios

Para llevar adelante este desarrollo se realizó una encuesta entre especialistas, docentes y usuarios de FPGA para conocer sus expectativas, preferencias, sugerencias y opiniones. Además, muchos de estos interesados colaboraron activamente aportando sugerencias y opiniones para el desarrollo de la plataforma. Estos profesionales pertenecen a diferentes universidades, instituciones y empresas:

 - UTN FR Buenos Aires
 - UTN FR Bahía Blanca
 - UTN FR San Nicolás
 - FIUBA
 - UNSAM
 - INTI
 - CITEDEF
 - Satellogic
 - Jotatec

# EDU-FPGA

## Resumen

![EDU-FPGA-LOGO](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/-/wikis/uploads/1a27ff0b03e42e60aa395881f77d3749/LOGO_3.png)

Como se explicó anteriormente, la placa **EDU-FPGA** fue diseñada con tres objetivos fundamentales en mente: _bajo costo, flexibilidad y facilidad de uso_. Está orientada a la enseñanza en nivel secundario, terciario y universitario, y también para cualquier persona interesada en adentrarse en el mundo FPGA.

## Bloques funcionales

La placa se basa en una FPGA [Lattice iCE40 HX4K](http://www.latticesemi.com/iCE40) a la cual se agregaron los minimos componentes necesarios para su funcionamiento y fácil utilización:
 - Memoria flash para almacenar la configuración de la placa
 - Interfaz FTDI para comunicar la placa con la PC mediante USB
 - Pulsadores y LEDs incorporados para probar ejemplos sencillos sin necesidad de componentes externos

### Diagrama en bloques de la plataforma

![EDU-FPGA-2](.images/EDU-FPGA-2.png)

#### Componentes de la placa

Como se mencionó antes, la FPGA elegida como núcleo de la plataforma es la [Lattice ICE40 HX4K TQ144](http://www.latticesemi.com/iCE40), la cual posee las siguientes especificaciones:

| **Lattice ICE40 HX4K TQ144**       |**Datos**|
| ---------------------------------- | ------- |
| Celdas Lógicas                     | 3520    |
| Bloques de Memoria RAM de 4Kbits   | RAM 20  | 
| Memoria Total                      | 80 Kbit |
| Phase Locking Loops (PLL)          | 2       |
| Bloques DSP                        | No      |
| Pines E/S                          | 95      |
| Pares Diferenciales E/S            | 12      |
| Encapsulado                        | TQ144   |
| Costo unitario                     | 6 USD   |

Al igual que la mayor parte de las FGPA, la Lattice ICE40 HX4K carece de memoria no volátil interna para almacenar su configuración. Por lo tanto, es necesario agregar una memoria flash que cumpla esta función. A tal fin, la EDU-FPGA v1.2 posee una memoria flash SPI [WinBond W25X40CL](https://www.winbond.com/resource-files/w25x40cl_f%2020140325.pdf) con las siguientes características:

| **WinBond W25X40CL**               |  **Datos**  |
| ---------------------------------- | ----------- |
| Tensión de Alimentación            | 2.3 a 3.6 V |
| Interfaz de Comunicación           | SPI         | 
| Frecuencia de Trabajo              | 104 MHz     |
| Capacidad                          | 4 MBit      |

La interfaz de configuración entre el conector USB que utiliza la placa para conectarse a la PC y la conexión SPI de la memoria flash y la FPGA se implementa a través de un chip FTDI [FT2232H](https://www.ftdichip.com/Support/Documents/DataSheets/ICs/DS_FT2232H.pdf). 

Finalmente, se incluyeron dos reguladores lineales de la serie [LD1117](https://www.st.com/en/power-management/ld1117.html) para proveer las tensiones de alimentación necesarias para los componentes:

| **Regulador** | **Tensión** |  **Alimenta a**                              |
|---------------|-------------|----------------------------------------------|
|LD1117S12      | 1.2 V       | Núcleo de la FPGA                            |
|LD1117S33      | 3.3 V       | E/S de la FPGA, memoria flash, interfaz FTDI |

#### Periféricos y pines de E/S

La placa cuenta con un total de **49 pines disponibles de entrada/salida de propósito general** (GPIO)conectados a los 4 bancos de E/S de la FPGA iCE40. Esta gran cantidad de conexiones disponibles le ofrece al usuario innumerables posibilidades, contribuyendo a la flexibilidad y escalabilidad de la plataforma.

Los pines de la FPGA trabajan a **3.3 V** y pueden manejar corrientes de hasta **6mA**. No todos los pines de la FPGA están conectados; aquellos pines accesibles en cada banco son:

| **Banco E/S de la FPGA** | **Pines accesibles**                     | **Cantidad total** |
|--------------------------|------------------------------------------|--------------------|
| Banco 0                  | 122, 124, 125, 128-130, 134-139, 141-144 | 16                 |
| Banco 1                  | 79-85, 95-99, 104-107                    | 16                 |
| Banco 2                  | 37                                       | 1                  |
| Banco 3                  | 7-14                                     | 16                 |
|                          | **TOTAL**                                | **49**             |

Por otro lado, la placa cuenta con un oscilador de cristal a 12 MHz, 4 pulsadores y 4 luces LEDs que sirven para para agilizar el desarrollo y ensayo de prototipos sin necesidad de conectar componentes adicionales. Estos elementos están conectados a los siguientes pines de la FPGA:

| **Componente** | **Pin de la FPGA** |
|----------------|--------------------|
| Cristal 12 MHz | 94                 |
| LED Verde      | 1                  |
| LED Rojo       | 2                  |
| LED Amarillo   | 3                  |
| LED Azul       | 4                  |
| Pulsadores     | 31, 32, 33 y 34    |

[Este diagrama](https://github.com/ciaa/Hardware/blob/master/PCB/EDU-FPGA/Pinout/Pinout%20EDU%20FPGA.pdf) muestra la placa y su distribución de pines de E/S.

### Estado actual 

Hasta ahora se han diseñado y producido dos versiones de la placa, las cuales pueden verse en la siguiente imagen : la **EDU-FPGA v1.0** (_a la izquierda_) y la **EDU-FPGA v1.2** (_a la derecha_).

![Versiones](.images/Versiones.jpeg)

Ambas fueron diseñadas por el Grupo de Aplicaciones en Sistemas Embebidos (ASE) de la [Universidad Tecnológica Nacional - Facultad Regional Haedo](https://www.frh.utn.edu.ar/). La versión 1.0 fue fabricada por el Grupo de Soldadura SMD de la misma facultad, mientras que la 1.2 fue fabricada por la empresa [Outsource](http://www.outsourcesrl.com/). Esta última versión incorpora algunas mejoras tales como pulsadores SMD y modelos más recientes de cristal y memoria.

Agradecemos la colaboración y revisión de los ingenieros Juan Manuel Cruz (Grupo Hasar, UTN FRBA, FIUBA), Nicolás Dassieu Blanchet (JOTATEC) y Christian Galasso (UTN-FRBB) por las sugerencias, revisión y aportes que realizaron durante todo el proceso de diseño y fabricación de los primeros prototipos.

Vale destacar que los PCB fueron desarrollados en base a las normas [IPC 2221A](http://www-eng.lbl.gov/~shuman/NEXT/CURRENT_DESIGN/TP/MATERIALS/IPC-2221A(L).pdf) e [IPC 7351](https://www.ipc.org/TOC/IPC-7351.pdf) para mejorar su manufacturabilidad.

El repositorio de Hardware del proyecto puede encontrarse en [este enlace](https://github.com/ciaa/Hardware/tree/master/PCB/EDU-FPGA). 

### Diseño del diagrama esquemático

Herramienta usada para el desarrollo (KiCAD), descarga en PDF de:
 - [Esquematico (v1.2)](https://github.com/ciaa/Hardware/blob/master/PCB/EDU-FPGA/Schematic/schematic.pdf)
 - [Diagrama pinout (v1.2)](https://github.com/ciaa/Hardware/blob/master/PCB/EDU-FPGA/Pinout/Pinout%20EDU%20FPGA.pdf)

### Listado de materiales

- [Descarga de BOM (v1.2) en formato ods](https://github.com/ciaa/Hardware/blob/master/PCB/EDU-FPGA/Schematic/BOM/BOM.ods)

### Placa de Circuito Impreso

- [Gerbers de fabricacion](https://github.com/ciaa/Hardware/tree/master/PCB/EDU-FPGA/Schematic/Gerbers)

---

# Imagenes de la EDU-FPGA en funcionamiento

> Probando pulsadores

![Pulsador](.images/Prueba_1.gif)

> Blinky

![Blinky](.images/Prueba_2.gif)

---

![UTN-FRH](http://www.frh.utn.edu.ar/static/img/logo-utn.jpg)
