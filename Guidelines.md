# Resumen - VHDL Guidelines

[[_TOC_]]

# Material de referencia

 - [Survey on VHDL Modelling Guidelines](https://www.researchgate.net/publication/270575219_Survey_on_VHDL_Modelling_Guidelines)
 - [ESA Coding Standards](http://web.cecs.pdx.edu/~mperkows/CLASS_VHDL_99/545_14-ESA-coding-standards.pdf)
 - [ESA VHDL Modelling Guidelines](hhttp://microelectronics.esa.int/vhdl/doc/ModelGuide.pdf)

 # Objetivo del documento

1. Resumir bibliografía de referencia sobre estilos de codificación para VHDL
2. Extraer las "buenas prácticas" comunes y/o mas extendidas
3. Crear un Guideline para la codificación de ejemplos en VHDL a usar en el repositorio

# Documentos

## Survey on VHDL Modelling Guidelines

### Abstract

> The goal of this paper is to clarify the state-of-the-art in the dynamic area of modelling guidelines
for VHDL. We distinguish between standards, de facto standards, and modelling rules stemming from
individual experiences. Further, we classify guidelines according to application domains with emphasis on
simulation. We relate guidelines to the IEEE standard VHDL. Then, we present shortly the main modelling
standards. The paper is summarized with an easy-to-use classification of the main modelling guidelines.

### Clasificacion de Guidelines

El paper propone que todos los Guidelines pueden clasificarse en:

1. **Estándares reales**: Aprobados por organismos de distintos tipos
    1. *Internacionales*: IEC, ISO
    2. *Regionales*: CENELEC, ANSI
    3. *Especializados*: IEEE, EIA, CFI, ECSI, OVI, VI
2. **Estándares de facto**: Definidos de forma interna por compañías u organizaciones como ESA 
3. **Estándares individuales**: Sugeridos por investigadores o desarrolladores particulares

Además, los Guidelines pueden aplicarse a uno o más de los siguientes elementos de desarrollo digital:
1. **Síntesis**: se busca obtener descripciones sintetizables en circuitos digitales o FPGA
2. **Simulación**: se busca simular descripciones circuitales digitales y/o analogicas
3. **Verificación**: se busca obtener testbenches exhaustivos para probar los diseños descriptos

Finalmente, los Guidelines pueden ser aplicables sobre distintos niveles de VHDL (o Verilog):
1. **Full language**: El Guideline puede usarse con todo el dominio del lenguaje
2. **Subset**: EL Guideline puede usarse con un subconjunto del lenguaje, usualmente limitado por una herramienta
3. **Extensiones**: Algunos Guidelines se refieren a extensiones del lenguaje, por ejemplo para circuitos analógicos

### Principales guidelines (en 1997 !!)

1. **VITAL**: VHDL Initiative Towards ASIC Libraries - aceptado por IEEE desde 1995 en el estándar IEEE 1076.4-1995
2. **OMI 326**: De facto VHDL Coding Standard
3. **ESA/ESTEC 1994 Modelling Guidelines** European Space Agency - se describe en el siguiente apartado
4. **EIA-567A**: Electronic Industries Association - VHDL Hardware Component Modeling and Interface Standard
5. **TIREP**: Technology Independent Representation of Electronic Products

## ESA Coding Standards

### Introducción

ESA es el acrónimo de la Agencia Espacial Europea (_European Space Agency_), que en 1994 publicó un documento de buenas prácticas para la documentación de diseños digitales.
Es importante remarcar que este manual esta orientado a la **documentación** de circuitos, y no a la elaboración de módulos **sintetizables**.Esta basado en VHDL-93, 
y fue elaborado en el [ESTEC](https://www.esa.int/About_Us/ESTEC/ESTEC_European_Space_Research_and_Technology_Centre) (_European Space Research and Technology Centre_)

### Guidelines básicos

#### Lenguaje
- Uso de un lenguaje común tanto para codigo como comentarios (inglés en el caso de la ESA)
(NOTA: VHDL esta en ingles)

#### Legibilidad
- Palabras reservadas del lenguaje: todo uppercase O todo lowercase (no mezclar)
- Identificadores (señales, labels, nombres, etc.):
    - Mixed case
    - No más de 15 caracteres
    - MÁXIMO 28 caracteres

```vhdl
-- All uppercase
ENTITY myCounter IS
    PORT(
       countClock  : IN  STD_LOGIC;
       countOutput : OUT STD_LOGIC_VECTOR(4 DOWNTO 0)
    );
END ENTITY myCounter;

-- All lowercase
entity myCounter is
    port(
       countClock  : in  std_logic;
       countOutput : out std_logic_vector(4 downto 0)
    );
end entity myCounter;
```

- Código conciso
- Eliminar código sin uso
- No más de un statement por línea
- No más de 80 caracteres por línea
- Usar indentación uniforme de 3 (o 4) espacios. **No usar TAB**
- Separar construcciones relacionadas mediante lineas en blanco y/o lineas de comentario (consistentemente)

```vhdl
-- New state logic
NewStateLogic : process (inputCommands,inputSensors,oldState) begin
   ...
end process;

-- Update state logic
UpdateState : process (clk,rst) begin
   ...
end process;

-----------------------------------------

-- Output logic
OutputLogic : process (oldState) begin
   ...
end process;
```

#### Convencion de nombres
- Establecimiento de una _naming convention_ uniforme (a criterio del equipo de diseño)
- La convencion debe estar documentada (preferentemente en cada archivo)
- Las señales activas en bajo deberían estar claramente señalizadas
- Indicar propósito, **no tipo** de la señal o el componente

```vhdl
-- GOOD:
signal acumCounter : std_logic_vector(7 downto 0);
-- BAD:
signal eightBitRegister: std_logic_vector(7 downto 0);
```

#### Comentarios en código
 - Deben explicar **CLARAMENTE** las funciones del módulo para que sean interpretables por otros diseñadores.
 - Colocar CERCA del código (no concentrar todos los comentarios en la cabecera)
 - Idem para subrprogramas o procesos

#### Cabecera
 - Debería contener:
    - Nombre de módulo(s) en el archivo
    - Nombre y propósito del archivo
    - Descripción, limitaciones y bugs conocidos del diseño (si corresponde)
    - Datos del autor
    - Dependencias y librerías necesarias
    - Plataforma en la que se probó el diseño

#### Testbenches
- **El testbench deberia ser realizado por alguien ajeno al diseño del módulo**
- El testbench debe cubrir la mayor cantidad de casos posibles (Full Code Coverage)
- Colocar testbenches y entidades en librerias separadas
- Utilizar assertions para identificar casos de interes en forma automatica

#### Otros
 - Solo usar archivos **Std.Textio.Txt**
 - Mantener los nombres de las señales y los ports para todos los niveles de jerarquia
 - Declarar los ports en orden : In -> Bidirectional -> Out
 - Usar un label descriptivo para todos los process. Cerrar el process con su propio label.
 - Usar preferentemente librerias aprobadas por IEEE

# Preguntas a futuro antes de elaborar nuestro guideline
1. ¿Existen otros guidelines más actualziados para adoptar?
2. ¿Existen guidelines aplicables a Verilog/System Verilog? 
3. ¿Existen guidelines ya aceptados y en uso a nivel nacional/regional?
4. ¿Se puede usar un guideline genérico como el ESA y aplicar criterios propios al grupo? - Si

# Guideline interno al repositorio de ejemplos del proyecto EDU CIAA FPGA

#### Lenguaje
 - Nombres y etiquetas en ingles para mantener uniformidad con el lenguaje
 - Comentarios en español para documentacion de los ejemplos con fines didacticos
 - Excepciones: terminos especificos en ingles
 - No usar **bit** o **bit_vector**. Usar **std_logic** o **std\_logic\_vector**.

#### Legibilidad
 - Mantener las palabras reservadas del lenguaje en minuscula
 - Para los identificadores (señales, labels, generics, nombres, etc.) usar camelCase.
 - Los nombres de las constantes deben ir en mayúscula.
 - Excepciones: siglas dentro del nombre en mayuscula
 - Mantener un codigo conciso, no dejar codigo comentado salvo a fines didacticos
 - No mas de un statement por linea
 - Listas de sensibilidad, mapeo de pines en componentes y otras listas:
    - Mantener en una misma linea si son 4 o menos elementos
    - Separar en una linea por elemento si son mas de 4
 - Listas de ports y generics al declarar una entidad: un elemento por linea
 - Usar una indentacion uniforme de 4 espacios por nivel. Cuidado con TAB.
 - Separar construcciones relacionadas dejando una linea en blanco
 - Separar grupos de contrucciones no relacionadas dejando una linea de comentario
 - Terminar cada definicion de process, entity o architecture con su nombre
 - Usar **downto** en **std_logic_vector**
 - Declarar y asignar ports en orden : In -> Bidirectional -> Out

```vhdl
--Comentario
entity myCamelCaseModule is
    port (
        count_in    : in  std_logic;
        count_out   : out std_logic_vector(4 downto 0);
        dataROM_out : out std_logic_vector(7 downto 0)
    );
end entity myCamelCaseModule;

...

architecture myCamelCaseModule_arch of myCamelCaseModule is
begin
...
end architecture myCamelCaseModule_arch;
----------------------------------------------------------------

-- Construcciones relacionadas entre ellas pero no con lo anterior

-- Proceso con menos de 4 elementos en la lista de sensibilidad
lessThanFour : process (clk_in,rst_in,countClock_in) begin
    ...
end process; -- lessThanFour

-- Proceso con mas de 4 elementos en la lista de sensibilidad
moreThanFour : process ( clk_in,
                         rst_in,
                         countClock_in,
                         dividerErrorAlarm_s,
                         comparerErrorAlarm_s,
                         alarmROM_s ) begin
    ..
end process; -- moreThanFour
```

#### Convencion de nombres
 - Los nombres deben identificar el proposito o funcion de cada elemento, no su estructura
 - Mantener los nombres de las señales y los ports para todos los niveles de jerarquia
 - Se usaran los siguientes posfijos cuando corresponda para mejor documentacion

##### Para entradas y salidas

| Posfijo |          Significado        |     Ejemplo     |
|---------|-----------------------------|-----------------|
|  \_in   | Entrada de la entidad       |     clk_in      |
| \_out   | Salida de la entidad        |     led_out     |
| \_inout | Puerto bidireccional[^1]    |  commIO_inout   |
|  \_n    | Linea activa en 0           |     rst\_in_n   |

##### Para señales internas

| Posfijo |          Significado               |    Ejemplo      |
|---------|------------------------------------|-----------------|
| \_state | Estado de una FSM                  | arbiter_state   |
|  \_cnt  | Contador                           | cycles_cnt      |
| \_reg   | Registo (almacena informacion)     | addres_reg      |
| \_bus   | Bus (transmite informacion)        | instruction_bus |
| \_ena   | Linea de habilitacion (enable)     | write_ena       |
| \_dis   | Linea de deshabilitacion (disable) | read_dis        |
| \_rst   | Linea de reset interno             | controlLoop_rst |
| \_clk   | Linea de clock interno             | memWrite_clk    |
|  \_s    | Otras señales                      | genericLine_s   |


##### Para instancias, entidades y arquitecturas

| Posfijo |          Significado        |     Ejemplo     |
|---------|-----------------------------|-----------------|
|  \_i[^2]| Instancia de componente     |  flipFlopD_0    |
|  \_tb   | Entidad del testbench       |  flipFlopD_tb   |
| \_arch  | Arquitectura de una entidad | flipFlopD_arch  |

##### Para funciones y procedures
 - En estos casos se recomienda que el identificador de estos elementos empiece en mayuscula

| Posfijo |  Significado  |     Ejemplo     |
|---------|---------------|-----------------|
| \_func  | Funcion       | Checksum_func   |
| \_proc  | Procedure     | ManageClk_proc  |

[^1]: Se desaconseja su uso
[^2]: Siendo i un indice que identifica a cada instancia creada

##### Para entidades top-level
 - El nombre del archivo y/o entidad será el nombre del archivo y/o entidad base más el sufijo **top_**. Por ejemplo:
    - Para un registro paralelo-paralelo de entidad "regPIPO", la entidad del top-level se llamará "top_regPIPO" y su archivo "top_regPIPO.vhdl".

#### Comentarios en código
 - Agregar una unica linea de comentario para cada linea de codigo relevante
 - Agregar un header de no mas de 10 lineas de comentario antes de cada process,
function o procedure indicando su funcion o relevancia

#### Cabecera
 - Agregar una cabecera o header de comentario indicando:
    - Nombre de la entidad en el archivo (no deberia haber mas de una)
    - Proposito de la entidad o del archivo (por ejemplo, en un testbench)
    - Descripción, limitaciones y bugs conocidos del diseño (si corresponde)
    - Datos del/los autor/es y fecha de creacion/revision
    - Dependencias y librerías necesarias (preferentemente aprobadas por IEEE)
    - IP cores propios o de terceros usados - aclarar autoria en cada caso
 - Ademas, deben incluirse en forma explicita todas las librerias y modulos requeridas.

## Testbenching Guidelines
- El testbench deberia ser (idealmente) realizado por alguien ajeno al diseño del módulo
- Colocar testbenches y entidades en librerias separadas
- Utilizar assertions para identificar casos de interes en forma automatica
- Usar archivos para cargar o guardar estimulos mediante text.io cuando sea de interes
- Usar funciones o procedures para generar de forma automatica los estimulos cuando sea posible
- Identificar los casos a verificar y armar un testbench dedicado para cada caso
- Modularizar el codigo del testbench mediante funciones y procedures para su
reutilizacion en otros casos de prueba
- Para las señales que mapean al o los DUT, anteponer el prefijo **test_**:

**port map (someInput_in => test_someInput_in)**

- Utilizar el posfijo **\_v** para los nombres de las variables. Por ejemplo:
**contador\_v**.
- Utilizar la siguiente nomenclatura para los testbenches de una entidad:

**nombreDelDispositivoBajoPrueba\_tb\_casoVerificado**

Por ejemplo:
 - divider\_tb\_conCocienteSinResto
 - divider\_tb\_sinCocienteConResto
 - divider\_tb\_conCocienteConResto
 - divider\_tb\_medicionTiemposDivision
 - divider\_tb\_divisionesPorCero
 


### Convención de nombres para archivos de texto utilizados en los testbenches

A estos archivos se les pone el mismo nombre que el archivo fuente del testbench 
pero sin el "_tb" y se les agrega uno de los siguientes posfijos:

 | Posfijo    |          Significado                                    |               Ejemplo                             |
 |------------|---------------------------------------------------------|---------------------------------------------------|
 |  \_in      | Archivo solo con datos de estímulo                      |     divider\_divisionesPorCero\_in.txt            |
 |  \_out     | Archivo solo con datos de salida de referencia          |     divider\_divisionesPorCero\_out.txt           |
 |  \_inouts  | Archivo con datos de estímulo y de salida de referencia |     divider\_divisionesPorCero\_inouts.txt        |

# Notas adicionales