# Repositorio de ejemplos de la EDU CIAA FPGA

## 1. Introducción

El objetivo de este repositorio es introducir al usuario de la EDU CIAA FPGA en el diseño, verificación y síntesis de circuitos digitales por medio de lenguajes de descripción de hardware. Los ejemplos se encuentran desarrollados en un orden de dificultad creciente para facilitar la gradualidad del aprendizaje.

Cada uno de los ejemplos es un proyecto que incluye, además del código fuente, una información detallada de las implicancias técnicas del diseño, el ensayo y en algunos casos la síntesis.

## 2. Índice de ejemplos

* [Compuerta AND](./Ejemplos_Base/00-Compuerta_AND)
* [Comparador 1 bit](./Ejemplos_Base/01-Comparador_1bit)
* [Multiplexor de 8 canales y N bits](./Ejemplos_Base/02-MUX_8CH_nbits)
* [Comparador de N bits](./Ejemplos_Base/03-Comparador_nbits)
* [Conversor BCD a 7 segmentos](./Ejemplos_Base/04-BCD_a_7seg)
* [Flip flop D](./Ejemplos_Base/05-FlipFlop_D)
* [Contador universal](./Ejemplos_Base/06-Contador_universal)
* [Registro paralelo-paralelo](./Ejemplos_Base/07-Registro_pp)
* [Registro serie-serie](./Ejemplos_Base/08-Registro_ss)
* [Registro paralelo-serie](./Ejemplos_Base/09-Registro_ps)
* [Registro serie-paralelo](./Ejemplos_Base/10-Registro_sp)
* [Oscilador controlado numéricamente (NCO)](./Ejemplos_Base/11-NCO)
* [Detector de trama](./Ejemplos_Base/12_Detector_de_trama)
* [Circuito antirebote](./Ejemplos_Base/13-Antirebote)
* [Árbitro](./Ejemplos_Base/14-Arbiter)
* [Conversor BCD a 7 segmentos con testbench mejorado](./Ejemplos_Base/16-BCD_a_7seg_TestbenchMejorado)
* [Oscilador controlado numericamente con LUT de salida](./Ejemplos_Base/19-NCO_LUT)
* [Oscilador controlado numericamente con LUT de salida e interfaz serial](./Ejemplos_Base/20-NCO_LUT_UART)
* [Periférico SPI](./Ejemplos_Base/23-SPI)
* [ADC AD7705](./Ejemplos_Base/24-AD7705)
* [Periférico serial](./Ejemplos_Base/Puerto_COM)

## 3. Links de interés

* [Página oficial](https://bit.ly/EDUFPGAok)
* [Canal de Youtube](https://bit.ly/EDUFPGAyoutube)
* [Wiki](https://bit.ly/EDUFPGAwiki)
