[[_TOC_]]

# Descripción

**Autor:** Ramiro A. Ghignone

El objetivo de este ejemplo es mostrar cómo se conjugan distintas estructuras como divisores de frecuencia, contadores y comparadores para generar formas de onda mediante hardware reconfigurable.

# Diagrama en bloques

![Bloques](.images/Bloques.png)

El diseño consta de:
1. Una entrada de clock conectada al cristal de 12 MHz de la placa.
2. Un prescaler configurable que divide la frecuencia de entrada por $`2^{N+1}`$, siendo N el parámetro dado por el bloque **Prescale**
3. Una cadena de ocho Flip Flops tipo T (TFF) en cadena, que forman un contador de 7 bits.
4. Un bloque de código en Verilog describiendo un detector de **umbral**.

# Cómo probar el ejemplo

1. Abrir **iCEstudio**
2. Abrir el archivo **3_EDU_CIAA_FPGA.ice**
3. Conectar la **EDU-CIAA-FPGA** a la PC
4. **Verificar** el diseño (usando **Ctrl+R** o el botón en la esquina inferior derecha)
5. **Sintetizar** el diseño (usando **Ctrl+B** o el botón en la esquina inferior derecha)
6. **Cargar** el diseño en la placa (usando **Ctrl+U** o el botón en la esquina inferior derecha)
7. Observar el patrón de destello del LED 0 ¿Hay mucha diferencia entre el tiempo que pasa encendido y que pasa apagado?

# Yo te propongo...

## Modificar el ancho de pulso
1. Reducir el parámetro **Umbral** a 20 ¿Cómo cambia el tiempo que el LED pasa encendido o apagado? 
2. Aumentar el parámetro **Umbral** a 200 ¿Cómo cambia el tiempo que el LED pasa encendido o apagado? 

## Modificar la frecuencia de salida
1. Sabiendo que el prescaler divide su frecuencia de entrada por $`2^{N+1}`$, calcular la frecuencia que entra a la cadena de TFFs.
2. Con el parámetro **Umbral** en 200, reducir el parámetro **Prescale** a 10. ¿Cuál es la nueva frecuencia con este valor de N?
3. Con la nueva frecuencia, reducir el parámetro **Umbral** a 2. ¿Qué ocurre con el brillo del LED?¿Por qué?

# Enlaces relacionados

 - [Descripción de una cadena de flip-flops en VHDL](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/-/wikis/Descripci%C3%B3n-estructural-en-VHDL)
 - [Descripción de una cadena de flip-flops en Verilog](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/wikis/Descripci%C3%B3n-estructural-en-Verilog)
 - [¿Qué usan las FPGA para gestionar su clock?](https://gitlab.com/RamadrianG/wiki---fpga-para-todos/wikis/PLL)
 - [Descripción y simulación de un comparador en VHDL](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base/03-Comparador_nbits)

No te olvides de revisar el [repositorio de ejemplos](https://gitlab.com/educiaafpga/ejemplos/-/tree/master/Ejemplos_Base) para ver otros diseños con uso de clock en VHDL.


