-- Librerias de VUnit para VHDL
library vunit_lib;
context vunit_lib.vunit_context;

-- Entidad del Testbench
entity tb_combinado is
  -- VUnit necesita este string en TODOS los Testbench:
  generic (runner_cfg : string);
end entity;

-- Arquitectura del Testbench
architecture tb of tb_combinado is
begin

  main : process
  begin
    test_runner_setup(runner, runner_cfg);

    -- Se hace un recorrido por varios tests distintos
    while test_suite loop

      -- Primer test : pasa
      if run("test_pass") then
        report "This will pass";

      -- Segundo test : no pasa
      elsif run("test_fail") then
        assert false report "You shall not pass !!!" severity error;

      end if;
    end loop;

    test_runner_cleanup(runner);
  end process;

end architecture;
