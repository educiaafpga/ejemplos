--Multiplexor de 2 canales (1 bit de seleccion)
--El nro de bits en cada canal es configurable (BITS_PER_CH)

library IEEE;
use IEEE.std_logic_1164.all;  

entity MUX_2CH is
    
    --Bits por canal
    generic(BITS_PER_CH : integer := 1);

    --Puertos de E/S
    port(   ch0     : in  std_logic_vector(BITS_PER_CH-1 downto 0);
            ch1     : in  std_logic_vector(BITS_PER_CH-1 downto 0);
            sel     : in  std_logic;
            out_ch  : out std_logic_vector(BITS_PER_CH-1 downto 0)  );

end entity MUX_2CH ;

architecture ARQ_MUX_2CH of MUX_2CH is

signal zeros : std_logic_vector(BITS_PER_CH-1 downto 0);

begin

    --Si la linea de seleccion va a un estado no definido, la salida va a cero
    zeros<=(others=>'0');

    process (ch0,ch1,sel) begin 
    
        case sel is
            when '0' => out_ch <= ch0;
            when '1' => out_ch <= ch1;
            when others => out_ch <= zeros;
        end case;

    end process;

end architecture ARQ_MUX_2CH ;
